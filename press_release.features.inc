<?php
/**
 * @file
 * press_release.features.inc
 */

/**
 * Implements hook_node_info().
 */
function press_release_node_info() {
  $items = array(
    'press_release' => array(
      'name' => t('Press release'),
      'base' => 'node_content',
      'description' => t('Used to store press releases and present them in both single node pages, listing views and sidebar widgets.'),
      'has_title' => '1',
      'title_label' => t('Header'),
      'help' => '',
    ),
  );
  return $items;
}
